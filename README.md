# README for Søren's vimfiles

The files are simply put directly into your local `.vim`/ folder.

Plugins are put in the `.vim/pack/` folder as per the new package standard.


## Installation

    $ git clone gitlab.com:zare88/vim ~/.vim

There are a few dependencies:

- FZF (install with you favourite package manager. Ie. `sudo port install fzf`)

    $ git submodule update --init --recursive

## Plugins

All plugins (including themes) are installed as Git sub-modules.

### A new plugin

To add a new plugin go into the ~/.vim/ dir and run a command similar to this:

`git submodule add github.com:fatih/vim-go pack/plugins/start/vim-go`

### update plugins

See `upgrade_vim_packs.sh` which basically just do `git submodule update --remote --merge`

